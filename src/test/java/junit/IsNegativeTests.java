package junit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsNegativeTests extends BeforeTests{
    @ParameterizedTest
    @ValueSource(longs = {9L, 738555393948834L, 0L})
    void longIsNegativeFunctionTestExpectedFalse(long a){
        boolean actualResult = obj.isNegative(a);
        assertFalse(actualResult, "Invalid result of isPositive, expected: false, actual: " + actualResult);
    }

    @ParameterizedTest
    @ValueSource(longs = {-9L, -738555393948834L})
    void longIsNegativeFunctionTestExpectedTrue(long a){
        boolean actualResult = obj.isNegative(a);
        assertTrue(actualResult, "Invalid result of isPositive, expected: true, actual: " + actualResult);
    }
}


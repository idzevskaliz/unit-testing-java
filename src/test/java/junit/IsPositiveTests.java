package junit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsPositiveTests extends BeforeTests{
    @ParameterizedTest
    @ValueSource(longs = {9L, 738555393948834L})
    void longIsPositiveFunctionTestExpectedTrue(long a){
        boolean actualResult = obj.isPositive(a);
        assertTrue(actualResult, "Invalid result of isPositive, expected: true, actual: " + actualResult);
    }

    @ParameterizedTest
    @ValueSource(longs = {-9L, -738555393948834L, 0L})
    void longIsPositiveFunctionTestExpectedFalse(long a){
        boolean actualResult = obj.isPositive(a);
        assertFalse(actualResult, "Invalid result of isPositive, expected: false, actual: " + actualResult);
    }
}

package testng;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleSumFunction")
    public Object[][] dataProvider(){
        return new Object[][]{
                {3, 2, 5},
                {0,0,0},
                {-2, -5, -7}
        };
    }
    @Test(dataProvider = "dataForDoubleSumFunction", groups = {"summarize"})
    public void doubleSumFunctionTest(double a, double b, double expectedResult){
        double actualResult = obj.sum(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of subdivide, expected: " + expectedResult + "actual: " + actualResult);
    }
    @DataProvider(name = "dataForLongSumFunction")
    public Object[][] dataForLongSum(){
        return new Object[][]{
                {365444444466465L, 2448754837848L, 367893199304313L},
                {0,0,0},
                {-365444444466465L, -2448754837848L, -367893199304313L}
        };
    }
    @Test(dataProvider = "dataForLongSumFunction", groups = {"summarize"})
    public void longSumFunctionsTest(long a, long b, long expectedResult){
        double actualResult = obj.sum(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of subdivide, expected: " + expectedResult + "actual: " + actualResult);
    }

}

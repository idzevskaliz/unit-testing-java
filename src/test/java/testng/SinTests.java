package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SinTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleSinFunction")
    public Object[][] dataForDoubleSin(){
        return new Object[][]{
                {9, Math.sin(9)},
                {-30, Math.sin(-30)},
                {144, Math.sin(144)},
                {0, Math.sin(0)}
        };
    }
    @Test(dataProvider = "dataForDoubleSinFunction", groups = {"trigonometry"})
    public void doubleSinFunctionTest(double a, double expectedResult){
        double actualResult = obj.sin(a);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of sin, expected: " + expectedResult + "actual: " + actualResult);
    }
}

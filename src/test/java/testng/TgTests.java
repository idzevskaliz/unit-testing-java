package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TgTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleTgFunction")
    public Object[][] dataForDoubleTg(){
        return new Object[][]{
                {9, Math.tan(9)},
                {-3, Math.tan(-3)},
                {25.4, Math.tan(25.4)},
                {0, Math.tan(0)}
        };
    }
    @Test(dataProvider = "dataForDoubleTgFunction", groups = {"trigonometry"})
    public void doubleTgFunctionTest(double a, double expectedResult){
        double actualResult = obj.tg(a);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of tg, expected: " + expectedResult + "actual: " + actualResult);
    }
}

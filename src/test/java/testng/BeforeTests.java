package testng;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;

public class BeforeTests {
    Calculator obj;

    @BeforeMethod(alwaysRun=true)
    public void setUp(){
        obj = new Calculator();
    }
}

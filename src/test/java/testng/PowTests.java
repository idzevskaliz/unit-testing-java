package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PowTests extends BeforeTests{
    @DataProvider(name = "dataForDoublePowFunction")
    public Object[][] dataForDoublePow(){
        return new Object[][]{
                {3, 2, 9},
                {6.4, 3, Math.pow(6.4, 3)},
                {-4.2, 2, Math.pow(-4.2, 2)},
                {5, 3.6, Math.pow(5, 3.6)},
                {2, 0, Math.pow(2,0)}
        };
    }
    @Test(dataProvider = "dataForDoublePowFunction", groups = {"degree"})
    public void doublePowFunctionTest(double a, double b, double expectedResult){
        double actualResult = obj.pow(a, b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of pow, expected: " + expectedResult + "actual: " + actualResult);
    }
}

package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleDivFunction")
    public Object[][] dataForDoubleDiv(){
        return new Object[][]{
                {3, 2, 1.5},
                {0,3,0.0}
        };
    }
    @Test(dataProvider = "dataForDoubleDivFunction", groups = {"division"})
    public void doubleDivFunctionTest(double a, double b, double expectedResult){
        double actualResult = obj.div(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of division, expected: " + expectedResult + "actual: " + actualResult);
    }

    @DataProvider(name = "dataForLongDivFunction")
    public Object[][] dataForLongDiv(){
        return new Object[][]{
                {6L, 2L, 3L},
                {0L,3L,0L},
        };
    }

    @Test(dataProvider = "dataForLongDivFunction", groups = {"division"})
    public void longDivFunctionTest(long a, long b, long expectedResult){
        long actualResult = obj.div(a, b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of division, expected: " + expectedResult + "actual: " + actualResult);
    }
    @Test(expectedExceptions = { NumberFormatException.class}, groups = {"division"})
    public void longZeroDivFunctionTest() throws Exception{
        obj.div(5L,0L);
    }
    @Test(expectedExceptions = { NumberFormatException.class}, groups = {"division"})
    public void doubleZeroDivFunctionTest() throws Exception{
        obj.div(5.0,0.0);
    }
}

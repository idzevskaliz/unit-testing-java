package testng;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SqrtTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleSqrtFunction")
    public Object[][] dataForDoubleSqrt(){
        return new Object[][]{
                {9, Math.sqrt(9)},
                {-84, Math.sqrt(Math.abs(-84))},
                {0, Math.sqrt(0)}
        };
    }
    @Test(dataProvider = "dataForDoubleSqrtFunction", groups = {"degree"})
    public void doubleSqrtFunctionTest(double a, double expectedResult){
        double actualResult = obj.sqrt(a);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of sqrt, expected: " + expectedResult + "actual: " + actualResult);
    }
}

package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CosTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleCosFunction")
    public Object[][] dataForDoubleCos(){
        return new Object[][]{
                {9, Math.cos(9)},
                {-30, Math.cos(-30)},
                {144.55, Math.cos(144.55)},
                {0, Math.cos(0)}
        };
    }
    @Test(dataProvider = "dataForDoubleCosFunction", groups = {"trigonometry"})
    public void doubleCosFunctionTest(double a, double expectedResult){
        double actualResult = obj.cos(a);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of cos, expected: " + expectedResult + "actual: " + actualResult);
    }
}

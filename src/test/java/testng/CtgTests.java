package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CtgTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleCtgFunction")
    public Object[][] dataForDoubleCtg(){
        return new Object[][]{
                {9, 1/Math.tan(9)},
                {-3, 1/Math.tan(-3)},
                {25, 1/Math.tan(25)},
                {0, 1/Math.tan(0)}
        };
    }
    @Test(dataProvider = "dataForDoubleCtgFunction", groups = {"trigonometry"})
    public void doubleCtgFunctionTest(double a, double expectedResult){
        double actualResult = obj.ctg(a);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of ctg, expected: " + expectedResult + "actual: " + actualResult);
    }
}

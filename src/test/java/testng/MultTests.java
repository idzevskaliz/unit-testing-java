package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleMultFunction")
    public Object[][] dataForDoubleMult(){
        return new Object[][]{
                {3, 2, 6.0},
                {0,0,0.0},
                {-2, -5, 10.0},
                {1.5, 3.4, 5.1}
        };
    }
    @Test(dataProvider = "dataForDoubleMultFunction", groups = {"multiply"})
    public void doubleMultFunctionTest(double a, double b, double expectedResult){
        double actualResult = obj.mult(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of multiply, expected: " + expectedResult + "actual: " + actualResult);
    }

    @DataProvider(name = "dataForLongMultFunction")
    public Object[][] dataForLongMult(){
        return new Object[][]{
                {957849, 243445354, 233183888883546L},
                {0,0,0},
                {-287834, -5345566, 1538635644044L}
        };
    }
    @Test(dataProvider = "dataForLongMultFunction", groups = {"multiply"})
    public void longMultFunctionTest(long a, long b, long expectedResult){
        long actualResult = obj.mult(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of multiply, expected: " + expectedResult + "actual: " + actualResult);
    }
}

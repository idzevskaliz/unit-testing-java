package testng;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SubTests extends BeforeTests{
    @DataProvider(name = "dataForDoubleSubFunction")
    public Object[][] dataForDoubleSub(){
        return new Object[][]{
                {3, 2, 1.0},
                {0,0,0.0},
                {-2, -5, 3.0}
        };
    }

    @Test(dataProvider = "dataForDoubleSubFunction", groups = {"subdivide"})
    public void doubleSubFunctionTest(double a, double b, double expectedResult){
        double actualResult = obj.sub(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of subdivide, expected: " + expectedResult + "actual: " + actualResult);
    }

    @DataProvider(name = "dataForLongSubFunction")
    public Object[][] dataForLongSub(){
        return new Object[][]{
                {365444444466465L, 2448754837848L, 362995689628617L},
                {0,0,0},
                {-365444444466465L, -2448754837848L, -362995689628617L}
        };
    }
    @Test(dataProvider = "dataForLongSubFunction", groups = {"subdivide"})
    public void longSubFunctionTest(long a, long b, long expectedResult){
        long actualResult = obj.sub(a,b);
        Assert.assertEquals(actualResult, expectedResult, "Invalid result of subdivide, expected: " + expectedResult + "actual: " + actualResult);
    }
}
